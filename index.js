const CliFrames = require("cli-frames");
const gifFrames = require('gif-frames');
const jpeg = require('jpeg-js');
const asciiPixels = require('ascii-pixels');
const sharp = require('sharp');
const async = require('async');

const frames = [];

let url = "./test.gif";
let contrast = 32;

if (process.argv[2]) {
    url = process.argv[2];
}

if (process.argv[3]) {
    contrast = parseInt(process.argv[3]);
    if (contrast > 255) {
        contrast = 255;
    }
    if (contrast < -255) {
        contrast = -255;
    }
}

gifFrames({ url: url, frames: 'all' }).then(function (frameData) {
    let multiplier = 1;
    let aux = Math.round(frameData[0].frameInfo.height / multiplier);
    while (aux > 70) {
        multiplier++;
        aux = Math.round(frameData[0].frameInfo.height / multiplier);
    }
    async.each(frameData, function (frame, callback) {
        let height = Math.round(frame.frameInfo.height / multiplier);
        let width = Math.round(frame.frameInfo.width / multiplier);
        sharp(frame.getImage()._obj)
            .resize(width, height)
            .toBuffer()
            .then(data => {
                let imageData = jpeg.decode(data)

                let ascii = asciiPixels(imageData, { contrast: contrast })
                frames.push(ascii);
                callback();
            });
    }, function (err) {
        if (err) {
            console.error(err);
        } else {
            let animation = new CliFrames();
            animation.load(frames);
            animation.start({
                repeat: true,
                delay: 40
            });
        }
    });
});